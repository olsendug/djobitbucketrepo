import argparse
import json
import sys

VERSION_STRING = "0.1.0"

def main(cli_args):
    '''
    Main function for the script
    @params cli_args: List of command line arguments.  This can be created by
        taking the command arguments and splitting them on the space character
    '''
    parser = argparse.ArgumentParser(description='Converts between SCR files and test JSON files')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s ' + VERSION_STRING)

    parser.add_argument('in_path', help='The path to the SCR or test JSON file to convert')
    parser.add_argument('out_path', help='The path to the output file to create')
    parser.add_argument('-d', '--dsp', default='left', help='DSP device values to include in test JSON files')
    
    args = parser.parse_args(cli_args)
    #convert(args.in_path, args.out_path, args.dsp)
    print(f'in_path = {args.in_path}.')
    print(f'out_path = {args.out_path}.')
    print(f'dsp = {args.dsp}.')

if __name__ == '__main__':
    try:
        # Don't pass in first argument (this filename)
        main(sys.argv[1:])
    except RuntimeError as err:
        print('ERROR: ' + err.args[0])
        sys.exit(1)